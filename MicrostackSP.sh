#!bin/bash/
##mettre a jour ubuntu et telecharger le package
sudo apt update  
sudo snap install microstack --devmode --beta
##installer microstack
sudo microstack init --auto --control
##recuperer le mot de passe par defaut 
echo "+++++++++: mot de pass"
sudo snap get microstack config.credentials.keystone-password
sudo iptables -t nat -A POSTROUTING -s 10.20.20.1/24 ! -d 10.20.20.1/24 -j MASQUERADE
sudo sysctl net.ipv4.ip_forward=1
###Ajouter les security group (ports :8069,9000,3000,8384,8080,443,9200,9100)
#+++ingress+++microstack.openstack security group rule create --prefix 0.0.0.0/0 --dst-port 8080:9000 --protocol udp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 3000 --protocol tcp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 8069 --protocol tcp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 8384 --protocol tcp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 9000 --protocol tcp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 9100 --protocol tcp --ingress --ethertype ipv4 default
microstack.openstack security group rule create --dst-port 9200 --protocol tcp --ingress --ethertype ipv4 default
#+++ingress+++microstack.openstack security group rule create --prefix 0.0.0.0/0 --dst-port 8080:9000 --protocol udp --ingress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 3000 --protocol tcp --egress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 8069 --protocol tcp --egress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 8384 --protocol tcp --egress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 9000 --protocol tcp --egress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 9100 --protocol tcp --egress --ethertype ipv4 default
#microstack.openstack security group rule create --dst-port 9200 --protocol tcp --egress --ethertype ipv4 default
###### Creation d'image ubuntu
microstack.openstack image create ubuntu --file bionic-server-cloudimg-amd64.img --public --tag ubuntu --min-ram 1024 --disk-format qcow2 --min-disk 10
microstack.openstack image list
###### Creation du Gabarit ds1024M du script projet 
microstack.openstack flavor create ds1024M --ram 1024 --disk 10 --vcpus 1

echo "pour l'interface graphique c'est le : http://10.20.20.1/   login :admin Passwd = Regarder en haut "
